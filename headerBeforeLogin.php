<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo">
            </div>



            <div class="right-menu-div float-right">
            	<a class="pink-hover-text menu-margin-right menu-item open-login">
                	<?php echo _HEADERBEFORELOGIN_LOGIN ?>
                </a>          
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect">
                    
                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->
                	
                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                	</div>
                </div>  

                                       	
            </div>
        </div>

</header>
