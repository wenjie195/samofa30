<?php


require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';





function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Register | Samofa 莎魔髪" />
    <title>Register | Samofa 莎魔髪</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding menu-distance75">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_REGISTER ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <!-- <form> -->
 	<form action="utilities/registerFunction.php" method="POST">
 		<div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_COUNTRY ?>" id="register_country" name="register_country" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="register_firstname" name="register_firstname" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="register_lastname" name="register_lastname" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_USERNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_EMAIL ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" id="register_email" name="register_email" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_PHONE ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_PHONE ?>" id="register_mobileno" name="register_mobileno" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_SPONSOR_ID ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_SPONSOR_ID ?>" id="sponsor_id" name="sponsor_id" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_ENROLLMENT_PRODUCT ?></p>
        	<input class="clean de-input" type="text" placeholder="Enrollment Product" id="register_product" name="register_product" required>
		</div>
        <div class="clear"></div>
        <div class="width100 text-center top-bottom-distance">
        	<button class="clean button-width transparent-button dark-pink-button" name="register"><?php echo _HEADERBEFORELOGIN_REGISTER ?></button>
        </div>
    </form>

</div>

	<div class="clear"></div>
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>