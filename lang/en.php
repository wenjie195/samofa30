<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
//JS
define("_JS_FOOTER", "@2020 Samofa, All Rights Reserved.");
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_RETYPE_REFERRER_NAME", "Referrer Name");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_MALAYSIA", "Malaysia");
define("_JS_SINGAPORE", "Singpore");
define("_JS_PHONE", "Phone No.");
define("_JS_REQUEST_TAC", "Request TAC");
define("_JS_TYPE", "Type");
define("_JS_SUBMIT", "Submit");
define("_JS_PLACEORDER", "Place Order");
define("_JS_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_JS_SUCCESS", "Success");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");
define("_JS_SPONSOR_ID", "Sponsor ID");
define("_JS_ENROLLMENT_PRODUCT", "Enrollment Product");
define("_JS_FLOWER", "Flower");
//HEADERBEFORELOGIN
define("_HEADERBEFORELOGIN_LOGIN", "Login");
define("_HEADERBEFORELOGIN_REGISTER", "Register");
//HEADER
define("_HEADER_ORDER", "Product");
//INDEX
define("_INDEX_QUOTE1", "Only by turning the mood of complaining about the environment into the power of progress, success can be guaranteed.");
define("_INDEX_QUOTE_AUTHOR1", "Romain Rolland");
define("_INDEX_QUOTE2", "First comes health, second personal beauty, then wealth honestly come by.");
define("_INDEX_QUOTE_AUTHOR2", "Plato");
define("_INDEX_QUOTE3", "As long as there is a belief and pursuit, women can endure any hardship and adapt to the environment.");
define("_INDEX_QUOTE_AUTHOR3", "Ding Ling");
define("_INDEX_SAMOFA", "Samofa");
define("_INDEX_CONTACT_NO", "Contact No");
define("_INDEX_EMAIL", "Email");
define("_INDEX_INTERESTED_IN_US", "Interested in us?");
define("_INDEX_OUR_PRODUCT", "Our Product");
define("_INDEX_MAGIC_DESC", "Your Fat Removal Magician");
define("_INDEX_MAGIC_DESC1", "Slimming + is a powder mix made by a combination of white kidney bean extract phase II, psyllium husk, inulin, multi-enzyme, konjac and aloe vera that work effectively in slimming and prevents yo-yo effects.");
define("_INDEX_HAIR_SERUM", "Purifying Balancing Shampoo & Serum Ultimate Elixir");
define("_INDEX_SERUM_DESC", "This is a deep repair wool scale hair care artifact. To restore the natural water balance, nourish and restore damaged hair.
");
define("_INDEX_QUANTITY", "Quantity");
define("_INDEX_ADD_TO_CART", "Add to Cart");
define("_INDEX_FUNCTION", "Functions & Benefits");
define("_INDEX_BLOCK_SUGAR", "Effectively blocks 66% sugar control");
define("_INDEX_BLOCK_CARB", "Effectively blocks 66% carbohydrates");
define("_INDEX_BLOCK_FAT_BURNING", "Fat Burning");
define("_INDEX_BLOCK_DETOX", "Detoxification");
define("_INDEX_ANTI_HUNGER", "Anti-Hunger");
define("_INDEX_USE_METHOD", "Use Method");
define("_INDEX_STEP1", "150ml～200ml room temperature water, mixed with 1sachet of MAGICBLOCA, shake well and drink.");
define("_INDEX_STEP2", "Open the sachet of MAGICBLOCA directly and pour it into the mouth slowly.");
define("_INDEX_INGREDIENTS", "Click to View Ingredients");
define("_INDEX_MAIN_INGREDIENTS", "Main Ingredients");
define("_INDEX_MIX_FRUIT_POWDER", "Mix Fruit Powder");
define("_INDEX_MIX_FRUIT_POWDER_DESC", "Mixed with pomegranate, lemon and pineapple juice, they are rich in nutrients and consist of various vitamins. Besides, they have good health effects on the cardiovascular system and intestinal system of the human body, and also contain powerful anti-cancer effects.");
define("_INDEX_PSYLLIUM_HUSK", "Psyllium Husk");
define("_INDEX_PSYLLIUM_HUSK_DESC", "It can enhance the amount of dietary fiber and play an important role in regulating the health of the gastrointestinal tract. For those with a high incidence of diabetes, a high-fiber diet can prevent the onset of diabetes and reduce weight.");
define("_INDEX_INULIN", "Inulin");
define("_INDEX_INULIN_DESC", "Is a prebiotic that promotes the growth of beneficial bacteria in the intestine. Moderate intake of inulin can help improve hyperlipidemia, relieve constipation, and help lose weight.");
define("_INDEX_FLAVONE", "Flavone");
define("_INDEX_FLAVONE_DESC", "Bioflavonoids have a variety of biological living things, and have important functions such as antibacterial, anti-inflammatory, anti-mutation, antihypertensive, clearing heat and detoxifying, improving microcirculation, anti-tumor and anti-oxidation.");
define("_INDEX_MULTIENZYME", "Multi Enzyme");
define("_INDEX_MULTIENZYME_DESC", "Help the body to catabolize protein, starch and fat in the body, improve immunity, improve gastrointestinal digestion, promote metabolism, accelerate the elimination of metabolic waste and toxic substances in the body, delay aging, and prevent disease.");
define("_INDEX_KONJAC", "Konjac");
define("_INDEX_KONJAC_DESC", "It is for beauty, anti - aging, lowering blood pressure, etc, therefore it’s not only delicious but also very practical. Because it is low in fat, low in sugar, and low in calories, it is wonderful for treating diabetes and high blood pressure. In addition, the human digestive system does not have the ability to digest and absorb it, so it can help the peristalsis of the stomach and intestines.");
define("_INDEX_AMYLASE", "α - Amylase Inhibitor, α - AI");
define("_INDEX_LEARN_MORE", "Learn More");
define("_INDEX_SUCCESS", "<img src='img/quote1.png' class='quote-img' alt='Success' title='Success'>");
define("_INDEX_HEALTH", "<img src='img/quote2.png' class='quote-img' alt='Health' title='Health'>");
define("_INDEX_FAITH", "<img src='img/quote3.png' class='quote-img' alt='Faith' title='Faith'>");
define("_INDEX_CORE_VALUE", "Core Value");
define("_INDEX_PURSUE_EXCELLENCE", "Pursue Excellence");
define("_INDEX_CONFIDENT", "Confident");
define("_INDEX_TRUST", "Trust");
define("_INDEX_CHARMING", "Charming");
define("_INDEX_REBORN", "Reborn");
define("_INDEX_INTIMATE", "Intimate");
define("_INDEX_TRANSFORM", "Transform");
define("_INDEX_SECURE", "Secure");
define("_INDEX_COMPANY_PURPOSE", "Company Purpose");
define("_INDEX_BRINGING_WEALTH", "Bringing Wealth");
define("_INDEX_BETTER_COMMUNITY", "Build a Better Community");
define("_INDEX_TALENT", "Cultivate Talent");
define("_INDEX_SERVICES", "Our Services Provided");
define("_INDEX_VALUABLE_LIFE", "A Valuable Life");
define("_INDEX_CONSIDERABLE", "Considerable Income");
define("_INDEX_TEAM_TRAINING", "Team Training");
define("_INDEX_LEADERSHIP", "Leadership");
define("_INDEX_HEALTHY_BODY", "Healthy Body");
define("_INDEX_BUILD_CONFIDENCE", "Build Confidence");
define("_INDEX_IMPROVE_QUALITY", "Improve Quality of Life");
define("_INDEX_MORE_FREEDOM", "More Freedom");
//Userdashboard
define("_USERDASHBOARD_DASHBOARD", "Dashboard");
define("_USERDASHBOARD_DOWNLINE", "Downline");
define("_USERDASHBOARD_FEMALE", "Lady");
//Product Details
define("_PRODUCTDETAILS", "Product Details");
define("_PRODUCTDETAILS_NO", "No.");
define("_PRODUCTDETAILS_NAME", "Name");
define("_PRODUCTDETAILS_IMAGE", "Image");
define("_PRODUCTDETAILS_PRICE", "Price");
define("_PRODUCTDETAILS_STOCK", "Stock");
define("_PRODUCTDETAILS_STATUS", "Status");
define("_PRODUCTDETAILS_DESCRIPTION", "Description");
define("_PRODUCTDETAILS_HOT_TOP_SALES", "Hot Sales/Top Sales");