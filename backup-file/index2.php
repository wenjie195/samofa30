<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Poppifx" />
    <title>Poppifx</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="width100 same-padding menu-distance bg1">
    
    <h1 class="h1-title white-text text-center"><?php echo _JS_LOGIN ?></h1>
    <form action="utilities/loginFunction.php" method="POST">
        <input class="clean de-input" type="text" placeholder="Username" id="username" name="username" required>
        <input class="clean de-input" type="password" placeholder="Password" id="password" name="password" required>
        <div class="clear"></div>
        <button class="clean blue-button mid-button-width small-distance small-distance-bottom" name="loginButton">LOGIN</button>
        <div class="clear"></div>
    </form>
    <!-- <h1 class="h1-title white-text text-center"></h1>
    <a href="register.php">Register Here</a> -->

</div>

</body>
</html>